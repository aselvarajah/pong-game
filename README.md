File(s) I used:
imagine_font.ttf

Sources I used:
https://stackoverflow.com/questions/2709821/what-is-the-purpose-of-self
https://www.youtube.com/watch?v=C6VCPX09rQE
https://stackoverflow.com/questions/33714595/creating-the-ball-to-spawn-randomly
https://www.youtube.com/watch?v=bk22K1m0890
https://www.pygame.org/docs/tut/tom_games4.html

You will need the following to run the game:
Python (Version 3.6 preferrably)
Command prompt (will run automatically)
A PC/Laptop in a working condiiton


This code runs a multiplayer pong game that can be controlled by the W,A,S,D keys as well as the arrow keys.
The player that reaches 10 first is the winner. A player can gain a point only if his/her opposition fails to make contact with the ball.

The code is written in a way that focuses on each aspect of the game individually. There is individual coding for each player; this allows for individual settings to be applied to both sets of codes. There are general aesthetic codes that involve how big the frame is and what colour the text, paddle, ball and bakcground is. The remaining code merely determine the speed and size of the paddles and the ball in the game. I used various sources from youtube to make the code and therefore it may appear quite complex, but I understood what I entered it due to the explanation on the videos. I have included some of the other sources I used to help me.

To actually run the  game, follow the instructions below:
Go to Files, press shift on the 'rocket-ship' folder and right click on it to open the command window for it
Then perform the 'virtualenv venv' command for it (by simply entering the words 'virtualenv venv')
Afterwards, the command prompt should load and install this function
After that you can simply enter 'venv\Scripts\activate' on Windows or source 'venv/bin/activate' on Mac
This transfers you to a virtual environment
Once you are in here, there will be a <venv> in front of the command line
Then you should freeze your requirements by doing 'pip freeze>requirements.txt' (if you are on a Windows PC/Laptop)
Next all you have to do is 'pip install pygame'
This will install pygame so that you can run the game
Finally, if you enter 'python main.py' on the command prompt, you should be able to play the game

In the future, the paddle sizes could decrease as the game progresses so that it is more challenging for both players. Also, the game could have sound effects so that the users can have a even more enjoyable gaming experience.